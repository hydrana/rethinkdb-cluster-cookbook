# frozen_string_literal: true

#
# Cookbook:: rethinkdb-cluster
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

include_recipe 'rethinkdb-cluster::install'
include_recipe 'rethinkdb-cluster::configure'
include_recipe 'rethinkdb-cluster::systemd'
