# frozen_string_literal: true

#
# Cookbook:: rethinkdb-cluster
# Recipe:: configure
#
# Copyright:: 2022, The Authors, All Rights Reserved.

require 'ipaddr'

ruby_block 'grab network interface IP address' do
  action :run
  block do
    interface = node['rethinkdb']['network']['bind']

    unless node['network']['interfaces'].key?(interface)
      Chef::Log.fatal "rethinkdb-cluster: This node doesn't have " \
                      "a #{interface.inspect} network interface. The " \
                      'available network interfaces are ' \
                      "#{node['network']['interfaces'].keys.sort}.\nPlease " \
                      "update the default['rethinkdb']['network']['bind'] " \
                      'attribute accordingly and converge again this node.'
      raise
    end

    addresses = node['network']['interfaces'][interface]['addresses']

    detected = addresses.detect do |address|
      # `address` is an array of this form:
      #  - MAC address: ["08:00:27:93:8E:9C", {"family"=>"lladdr"}]
      #  - IPv4: ["172.28.128.200", {"family"=>"inet", ... }]
      #  - IPV6: ["fe80::a00:27ff:fecc:570a", {"family"=>"inet6", ... }]
      #
      # address.first returns the first array element, so "08:00:27:93:8E:9C",
      # "172.28.128.200" and "fe80::a00:27ff:fecc:570a" in this example.
      IPAddr.new(address.first).ipv4?
    rescue IPAddr::InvalidAddressError
      false
    end

    node.run_state['detected_address'] = detected && detected.first
  end

  only_if do
    node['rethinkdb'] && node['rethinkdb']['network'] && \
      node['rethinkdb']['network']['bind'].is_a?(String) && \
      node['rethinkdb']['network']['bind'] =~ /[[:alnum:]]/
  end
end

ruby_block 'look for other RethinkDB nodes' do
  action :run
  block do
    peers = search(:node, 'run_list:*rethinkdb-cluster\:\:default')

    detected_peer = peers.detect do |peer|
      Chef::Log.warn "rethinkdb-cluster: Peer tags are #{peer['rethinkdb']['server_tags'].inspect} and this node tags are #{node['rethinkdb']['server_tags'].inspect} ..."
      node['rethinkdb']['server_tags'] - peer['rethinkdb']['server_tags'] == []
    end

    if detected_peer
      cluster_port = detected_peer['rethinkdb'] &&
                     detected_peer['rethinkdb']['network'] &&
                     detected_peer['rethinkdb']['network']['ports'] &&
                     detected_peer['rethinkdb']['network']['ports']['cluster'] ||
                     nil

      Chef::Log.warn "rethinkdb-cluster: This node is seen as a secondary one!"

      node.run_state['join'] = [detected_peer['ipaddress'], cluster_port].join(':')
    else
      Chef::Log.warn "rethinkdb-cluster: This node is seen as the primary one!"
    end
  end
end

template '/etc/rethinkdb/instances.d/cluster.conf' do
  source 'rethinkdb.conf.erb'
  owner 'rethinkdb'
  group 'rethinkdb'
  mode '0644'
  variables(
    {
      bind: lazy do
              node.run_state['detected_address'] ||
              node['rethinkdb']['network']['bind']
            end,
      cert_pem_path: File.join(node['rethinkdb']['data_directory'], 'cert.pem'),
      join: lazy { node.run_state['join'] },
      key_pem_path: File.join(node['rethinkdb']['data_directory'], 'key.pem')
    }
  )
end

execute "Create RethinkDB data directory at #{node['rethinkdb']['data_directory']}" do
  action :run
  command "rethinkdb create --directory #{node['rethinkdb']['data_directory']}"
  user 'rethinkdb'

  not_if "test -d #{node['rethinkdb']['data_directory']}"
end

ruby_block 'deploy TLS certificats' do
  action :run
  block do
    data = data_bag_item('rethinkdb', 'tls')

    required_keys = %w[id cluster-cert cluster-key]

    unless required_keys - data.keys == []
      Chef::Log.fatal "rethinkdb-cluster: You've requested to secure " \
                      'RethinkDB cluster communications using a TLS ' \
                      'certificat but your rethinkdb data bag is laking ' \
                      'required keys. Please review it and ensure you have ' \
                      "the #{required_keys.join(', ')} keys (Missing key(s) " \
                      "is/are #{(required_keys - data.keys).join(', ')})"
      raise
    end

    File.open(File.join(node['rethinkdb']['data_directory'], 'cert.pem'),
              'w') do |cert|
      cert.write(data['cluster-cert'])
    end

    File.open(File.join(node['rethinkdb']['data_directory'], 'key.pem'),
              'w') do |key|
      key.write(data['cluster-key'])
    end
  end

  only_if { node['rethinkdb']['tls']['cluster'] == true }
end

execute "change owner and group for #{node['rethinkdb']['data_directory']}" do
  action :run
  command "chown -R rethinkdb:rethinkdb #{node['rethinkdb']['data_directory']}"
end
