# frozen_string_literal: true

#
# Cookbook:: rethinkdb-cluster
# Recipe:: install
#
# Copyright:: 2022, The Authors, All Rights Reserved.

execute 'imports RethinkDB repository GPG key' do
  command 'wget -qO- https://download.rethinkdb.com/repository/raw/pubkey.gpg | sudo apt-key add -'

  only_if do
    `bash -c 'apt-key list | grep "539A 3A8C 6692 E6E3 F69B  3FE8 1D85 E93F 801B B43F" | wc -l'` == "0\n"
  end
end

apt_repository 'rethinkdb' do
  uri "https://download.rethinkdb.com/repository/ubuntu-#{node['lsb']['codename']}"
  distribution node['lsb']['codename']
  components ['main']
end

package 'rethinkdb' do
  action :install
  version node['rethinkdb']['version']
end

execute 'change owner and group for /var/lib/rethinkdb/instances.d/' do
  action :run
  command 'chown -R rethinkdb:rethinkdb /var/lib/rethinkdb/instances.d/'
end
