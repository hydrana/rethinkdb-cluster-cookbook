# frozen_string_literal: true

#
# Cookbook:: rethinkdb-cluster
# Recipe:: systemd
#
# Copyright:: 2022, The Authors, All Rights Reserved.

template '/usr/lib/tmpfiles.d/rethinkdb.conf' do
  source 'rethinkdb.conf'
  owner 'rethinkdb'
  group 'rethinkdb'
  mode '0644'
end

systemd_unit 'rethinkdb@cluster.service' do
  content <<~EOU
  [Unit]
  Description=RethinkDB database server for instance '%i'

  [Service]
  User=rethinkdb
  Group=rethinkdb
  ExecStart=/usr/bin/rethinkdb serve --config-file /etc/rethinkdb/instances.d/%i.conf
  KillMode=process
  PrivateTmp=true

  [Install]
  WantedBy=multi-user.target
  EOU

  action [:create, :enable, :start]
end
