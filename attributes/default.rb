# frozen_string_literal: true

### Server process options

# Which version of RethinkDB would you like to install?
#
# Let it `nil` in order to install the latest version
default['rethinkdb']['version'] = nil

# RethinkDB server tags
#
# See https://rethinkdb.com/docs/sharding-and-replication/#server-tags
default['rethinkdb']['server_tags'] = nil

# RethinkDB data directory path
#
default['rethinkdb']['data_directory'] = '/var/lib/rethinkdb/instances.d/cluster'

# RehtinkDB Cluster secured with a TLS certificate
#
# When this is `true`, this cookbook will import your TLS certificate from a
# secured data bag, and use it to secure the cluster communications.
default['rethinkdb']['tls']['cluster'] = false

### Network options

# RethinkDB bind
#
# Address of local interfaces to listen on when accepting connections
# May be 'all' or an IP address, loopback addresses are enabled by default
# Default: all local addresses
#
# - When passing `nil`, the RethinkDB default is applied, therefore all local
# addresses.
# - When passing a string including only alphanumeric characters, this cookbook
# will look at the node's network interfaces with the given name and take its
# first inet/IPv4 address using.
# - When passing a string including symbols, therefore being an IPv4 address or
# an IPv6 address, this cookbook passes at is to the bind configuration option.
default['rethinkdb']['network']['bind'] = nil

# Disable RethinkDB Admin UI
#
# Sets this to `false` in order to start the admin UI
default['rethinkdb']['network']['disable-admin-ui'] = true

# RethinkDB Admin UI port
#
# By default RethinkDB Admin UI listens on 8080 + port-offset.
#
# You can give a specific port or let it `nil` if you'd like to apply
# the RethinkDB default.
default['rethinkdb']['network']['ports']['admin'] = nil

# RethinkDB cluster port
#
# By default RethinkDB cluster communications are made over
# the port 29015 + port-offset.
#
# You can give a specific port or let it `nil` if you'd like to apply
# the RethinkDB default.
default['rethinkdb']['network']['ports']['cluster'] = nil
