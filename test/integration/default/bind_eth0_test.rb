# frozen_string_literal: true

# Chef InSpec test for recipe rethinkdb-cluster::configure

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/etc/rethinkdb/instances.d/cluster.conf') do
  it { should exist }

  its('group') { should eq 'rethinkdb' }
  its('owner') { should eq 'rethinkdb' }
  its('mode') { should cmp '0644' }

  its('content') { should match(/bind=\d+\.\d+\.\d+\.\d+/) }
end
