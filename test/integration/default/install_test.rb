# frozen_string_literal: true

# Chef InSpec test for recipe rethinkdb-cluster::install

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/etc/apt/sources.list.d/rethinkdb.list') do
  it { should exist }
  its('content') { should match(%r{^deb\s+https://download.rethinkdb.com/repository/ubuntu-\w+\s\w+\smain\n$}) }
end

describe package('rethinkdb') do
  it { should be_installed }
end
