# frozen_string_literal: true

# Chef InSpec test for recipe rethinkdb-cluster::configure

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/usr/lib/tmpfiles.d/rethinkdb.conf') do
  it { should exist }

  its('group') { should eq 'rethinkdb' }
  its('owner') { should eq 'rethinkdb' }
  its('mode') { should cmp '0644' }

  its('content') { should eql(File.read(File.join('templates', 'rethinkdb.conf'))) }
end

describe file('/etc/systemd/system/rethinkdb@cluster.service') do
  it { should exist }

  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
  its('mode') { should cmp '0644' }

  its('content') do
    should eql(
      <<~REHINKDB_SERVICE
        [Unit]
        Description=RethinkDB database server for instance '%i'

        [Service]
        User=rethinkdb
        Group=rethinkdb
        ExecStart=/usr/bin/rethinkdb serve --config-file /etc/rethinkdb/instances.d/%i.conf
        KillMode=process
        PrivateTmp=true

        [Install]
        WantedBy=multi-user.target
      REHINKDB_SERVICE
    )
  end
end

describe systemd_service('rethinkdb@cluster') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
