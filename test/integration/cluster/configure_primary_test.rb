# frozen_string_literal: true

# Chef InSpec test for recipe rethinkdb-cluster::configure

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/etc/rethinkdb/instances.d/cluster.conf') do
  its('content') { should_not match(/^join=.*$/) }
end
