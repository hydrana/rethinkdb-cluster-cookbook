# frozen_string_literal: true

# Chef InSpec test for recipe rethinkdb-cluster::configure

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/etc/rethinkdb/instances.d/cluster.conf') do
  it { should exist }

  its('group') { should eq 'rethinkdb' }
  its('owner') { should eq 'rethinkdb' }
  its('mode') { should cmp '0644' }

  its('content') { should match(/server-tag=kitchen/) }
  its('content') { should match(/bind=\d+\.\d+\.\d+\.\d+/) }
  its('content') { should match(/cluster-port=29915/) }
  its('content') { should match(%r{^cluster-tls-ca=/var/lib/rethinkdb/instances.d/cluster/cert.pem$}) }
  its('content') { should match(%r{^cluster-tls-cert=/var/lib/rethinkdb/instances.d/cluster/cert.pem$}) }
  its('content') { should match(%r{^cluster-tls-key=/var/lib/rethinkdb/instances.d/cluster/key.pem$}) }
end

describe directory('/var/lib/rethinkdb/instances.d/cluster') do
  it { should exist }

  its('owner') { should eq 'rethinkdb' }
  its('group') { should eq 'rethinkdb' }
end

describe file('/var/lib/rethinkdb/instances.d/cluster/cert.pem') do
  it { should exist }

  its('group') { should eq 'rethinkdb' }
  its('owner') { should eq 'rethinkdb' }
  its('mode') { should cmp '0644' }

  its('content') { should match(/\A-----BEGIN CERTIFICATE-----\n.*\n-----END CERTIFICATE-----\n\z/m) }
end

describe file('/var/lib/rethinkdb/instances.d/cluster/key.pem') do
  it { should exist }

  its('group') { should eq 'rethinkdb' }
  its('owner') { should eq 'rethinkdb' }
  its('mode') { should cmp '0644' }

  its('content') { should match(/\A-----BEGIN RSA PRIVATE KEY-----\n.*\n-----END RSA PRIVATE KEY-----\n\z/m) }
end
