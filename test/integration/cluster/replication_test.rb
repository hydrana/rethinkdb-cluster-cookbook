# frozen_string_literal: true

# Chef InSpec test for rethinkdb-cluster

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe command('/usr/local/bin/rethinkdb-status') do
  it { should exist }
  its('exit_status') { should eq 0 }
  its('stdout') { should match(/^{'primary_rethinkdb_[a-z0-9]+': True}\n$/) }
end
