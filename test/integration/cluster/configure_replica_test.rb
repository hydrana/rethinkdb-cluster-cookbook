# frozen_string_literal: true

# Chef InSpec test for recipe rethinkdb-cluster::configure

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe file('/etc/rethinkdb/instances.d/cluster.conf') do
  its('content') { should match(/^join=primary.rethinkdb:29915$/) }
end
