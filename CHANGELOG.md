# rethinkdb-cluster CHANGELOG

This file is used to list changes made in each version of the rethinkdb-cluster cookbook.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2022-02-11
### Added
- Admin UI config to disable it and change port

## [0.2.0] - 2022-02-09
### Added
- Implements clustering configuration (port + TLS)

## [0.1.0] - 2022-02-02

Initial release.

[Unreleased]: https://gitlab.com/hydrana/rethinkdb-cluster-cookbook/-/compare/v0.3.0...master
[0.3.0]: https://gitlab.com/hydrana/rethinkdb-cluster-cookbook/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/hydrana/rethinkdb-cluster-cookbook/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/hydrana/rethinkdb-cluster-cookbook/-/tags/v0.1.0
