# rethinkdb-cluster

Installs RethinkDB and configures it in a cluster.

## Requirements

### Platforms

* Ubuntu 20.04 LTS

### Chef

* Chef >= 16

## Attributes

### Server process

| Name | Purpose | Default |
| ---- | ------- | ------- |
| `default['rethinkdb']['version']` | Choose the package version to be installed | `nil` meaning latest |
| `default['rethinkdb']['server_tags']` | Tags to be passed when running the server. See the [Server tags](https://rethinkdb.com/docs/sharding-and-replication/#server-tags) documentation page | `nil` meaning no tags |
| `default['rethinkdb']['data_directory']` | Server directory where data and logs are stored | `/var/lib/rethinkdb/instances.d/cluster` |
| `default['rethinkdb']['tls']['cluster']` | Should the cluster communication be secured using certificate from an encrypted data bag | `false` |

### Network

| Name | Purpose | Default |
| ---- | ------- | ------- |
| `default['rethinkdb']['network']['bind']` | Address of local interfaces to listen on when accepting connections (See attributes/default.rb`) | `nil` meaning RethinkDB default to all local addresses |
| `default['rethinkdb']['network']['ports']['cluster']` | RethinkDB port used for cluster communications | `nil` meaning RethinkDB default to 29015 + port-offset |
| `defautl['rethinkdb']['network']['disable-admin-ui']` | Disables RethinkDB Admin UI | `true` |
| `default['rethinkdb']['network']['ports']['admin']` | RethinkDB admin port | `nil` meaning RethinkDB default to 8080 + port-offset |

## Clustering

This cookbook leverages the Chef's `search` so that converging a first node will
initialize a RetinkDB cluster, while all other converged nodes, with the same
tags than the first one, will automatically join it automatically.

## Recipes

* `rethinkdb-cluster::default` : Installs and configure RethinkDB
* `rethinkdb-cluster::install` : Installs RethinkDB using the package manager

## Securing cluster communications

When your nodes doesn't have a secured private network, you must secure the
cluster communications using a TLS certificate.\
You can find more on the [RethinkDB documentation](https://rethinkdb.com/docs/security/).

In order to do so, you have the create a TLS certificate following the
instructions from the [RethinkDB documentation page](https://rethinkdb.com/docs/security#generate-a-key-and-matching-certificate).\
You should have a `cert.pem` and a 'key.pem' file.

You will have to store them inline so you need to get a single line version
usign the commands :
* `awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' cert.pem && echo`
* `awk 'NF {sub(/\r/, ""); printf "%s\\n",$0;}' key.pem && echo`

Now you need to create an encrypted data bag with named `rethinkdb` with the
item `tls`:

```
EDITOR=nano knife data bag create rethinkdb tls --secret-file ~/.chef/knife-secret
```

(The file `~/.chef/knife-secret` is a regular file including a simple string,
like a MD5, SHA256, RSA key etc). You have to upload that file on each nodes at
the path `/etc/chef/encrypted_data_bag_secret` which will allow Chef to decrypt
the data bag).

In the created data bag, add the `cluster-cert` and `cluster-key` attributes with
the inline version of the above generated files:

```
{
  "id": "tls",
  "cluster-cert": "-----BEGIN CERTIFICATE-----\nMIID...Uj4=\n-----END CERTIFICATE-----\n",
  "cluster-key": "-----BEGIN RSA PRIVATE KEY-----\nMIIE...hjU\n-----END RSA PRIVATE KEY-----\n"
}
```

Last but not least update the `default['rethinkdb']['tls']['cluster']` attribute
to `true` and converge.
